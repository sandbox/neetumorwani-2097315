<?php
/**
 * QuestionnaireEntityControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */

interface QuestionnaireEntityControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * QuestionnaireEntityController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class QuestionnaireEntityController
  extends DrupalDefaultEntityController
  implements QuestionnaireEntityControllerInterface {

  /**
   * Create and return a new questionnaire entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'questionnaire';
    $entity->qid = 0;
    $entity->bundle = 'questionnaire';
    $entity->choice = 1;
    $entity->criteria = 0;
    $entity->yes_quid = '';
    $entity->no_quid = '';
    $entity->tokenized_criteria = '';
    $entity->title = '';
    $entity->text = '';
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    if (empty($entity->qid)) {
      $entity->created = time();
    }

    module_invoke_all('entity_presave', $entity, 'questionnaire');

    $primary_keys = $entity->qid ? 'qid' : array();
    drupal_write_record('questionnaire', $entity, $primary_keys);
    $invocation = 'entity_insert';

    if (empty($primary_keys)) {
      field_attach_insert('questionnaire', $entity);
    }
    else {
      field_attach_update('questionnaire', $entity);
      $invocation = 'entity_update';
      //print_r($entity);exit;
    }
    module_invoke_all($invocation, $entity, 'questionnaire');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more questionnaire entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $qids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $qids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'questionnaire');
          field_attach_delete('questionnaire', $entity);
          $qids[] = $entity->qid;
        }
        db_delete('questionnaire')
          ->condition('qid', $qids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('questionnaire', $e);
        throw $e;
      }
    }
  }
}
