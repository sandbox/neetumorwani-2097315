<?php
/**
 *
 */
global $mapping;
$mapping = array (
  "radios" => "Radiobutton",
  "checkboxes" => "Checkbox",
  "select" => "Selectlist",
  );

function questionnaire_list_entities() {
  $content = array();

  $entities = entity_load('questionnaire');
  if (!empty($entities)) {
    /*echo "<pre>";print_r($entities);exit;*/
    foreach ( $entities as $entity ) {
      $rows[] = array(
        'data' => array(
          'id' => $entity->qid,
          'item_description' => l($entity->title, 'questionnaire/' . $entity->qid),
          'bundle' => $entity->bundle,
          ),
        );
    }

    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Question'), t('Bundle')),
      );
  }
  else {
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No Questionnaires currently exist.'),
      );
  }
  return $content;
}

/**
 *
 */
function questionnaire_add() {
  $entity = entity_get_controller('questionnaire')->create();
  return drupal_get_form('questionnaire_add_form', $entity);
}

/**
 *
 */
function questionnaire_add_form($form, &$form_state, $entity) {
   if($entity->qid)
   $edit = unserialize($entity->criteria_data);
   //dsm($edit);

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Questionnaire Title'),
    '#required' => TRUE,
    '#default_value' => $entity->title,
    );
  $form['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Questionnaire Body'),
    '#required' => TRUE,
    '#default_value' => $entity->text,
    );

  $questionnaire_type = 0;

  if ($entity->choice) {
    $questionnaire_type = 0;
  }
  else if ($entity->criteria) {
    $questionnaire_type = 1;
  }

  $form['questionnaire_type'] = array(
    '#type' => 'radios',
    '#options' => array('0' => 'Choice', '1' => 'Criteria'),
    '#default_value' => $questionnaire_type
    );


  $form['radio_field'] = array(
    '#type' => 'checkbox',
    '#title' => t(' Radio-Button'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="questionnaire_type"]' => array('checked' => FALSE),
        ),
      ),
    '#options' => array(
      0 => 'Radio Button',
      /*1 => 'No Radio button',*/
      /*2 => 'Select list',*/
      ),
    '#default_value' => empty($edit['radio_field']) ? 0 : $edit['radio_field'][0],
    '#ajax' => array(
      'callback' => 'ajax_append',
      'wrapper' => 'placeholder',
      //'method' => 'append',
      ),
    /*'#validated' => TRUE*/
    );
  /*$form['widget_radio_button'] = array(
    '#type' => 'radio',
    '#title' => t('Radio button'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="radio_field"]' => array('checked' => true),
        ),
      ),
    //  '#default_value' => 1, => array('value' => 'high_school'),
    );*/

  $form['textfield_radio'] = array(
    '#type' => 'textarea',
    '#title' => t(' Text-field for Radio button for evaluating the criteria'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="radio_field"]' => array('checked' => true),
        ),
      ),
    '#default_value' => empty($edit['radio_field']) ? null : $edit['radio_field'][1],
    );
  /*$form['checkbox_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checkbox'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="questionnaire_type"]' => array('checked' => FALSE),
        ),
      ),
    '#options' => array(
      0 => 'Check Box',

      ),
    '#default_value' => empty($edit['checkbox_field']) ? 0 : $edit['checkbox_field'][0],
    '#ajax' => array(
      'callback' => 'ajax_append',
      'wrapper' => 'placeholder',

      //'method' => 'append',
      )
    );*/

  /*$form['widget_checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Checkbox'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="checkbox_field"]' => array('checked' => true),
        ),
      ),
    //  '#default_value' => 1, => array('value' => 'high_school'),
    );

  $form['textfield_checkbox'] = array(
    '#type' => 'textarea',
    '#title' => t(' Text-field for checkbox for evaluating the criteria'),
    '#validated' => 'TRUE',
    '#states' => array(

      'visible' => array(
        ':input[name="checkbox_field"]' => array('checked' => true),
        ),
      ),
    '#default_value' => empty($edit['checkbox_field']) ? null : $edit['checkbox_field'][1],
    );*/

  $form['selectlist_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Select-list'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="questionnaire_type"]' => array('checked' => FALSE),
        ),
      ),
    '#options' => array(
      0 => 'Select list',
      /*1 => 'No Select List'*/
      /*1 => 'Checkbox',
      2 => 'Select list',*/
      ),
    '#default_value' => empty($edit['selectlist_field']) ? 0 : $edit['selectlist_field'][0],
    '#ajax' => array(
      'callback' => 'ajax_append',
      'wrapper' => 'placeholder',
      //'method' => 'append',
      )
    );

  /*$form['widget_selectlist'] = array(
    '#type' => 'select',
    '#title' => t('Select-list'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="selectlist_field"]' => array('checked' => true),
        ),
      ),
    //  '#default_value' => 1, => array('value' => 'high_school'),
    );*/

  $form['textfield_selectlist'] = array(
    '#type' => 'textarea',
    '#title' => t(' Text-field for Select-list for evaluating the criteria'),
    '#validated' => 'TRUE',
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="selectlist_field"]' => array('checked' => true),
        ),
      ),
    '#default_value' => empty($edit['selectlist_field']) ? null : $edit['selectlist_field'][1],
    );
  $form['tokenized_criteria'] = array(
    '#type' => 'textarea',
    '#title' => t('Enter the tokenized criteria to decide on yes/no option'),
    '#states' => array(
    // Only show this field when the 'toggle_me' checkbox is enabled.
      'visible' => array(
        ':input[name="questionnaire_type"]' => array('checked' => FALSE),
        ),
      ),
    '#default_value' => $entity->tokenized_criteria,
    '#prefix' => '<div id="placeholder">',
    '#suffix' => '</div>',
    );


  $form['yes_reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Which node should the user be taken to in case he clicks yes/the data entered by them passes the criteria'),
    '#autocomplete_path' => 'questionnaire/autocomplete',
    '#default_value' => $entity->yes_quid
    );

  $form['no_reference'] = array(
    '#type' => 'textfield',
    '#title' => t('Which node should the user be taken to in case he clicks no/the data entered by them fails the criteria'),
    '#autocomplete_path' => 'questionnaire/autocomplete',
    '#default_value' => $entity->no_quid
    );

  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
    );
  /*echo "<pre>";print_r($form['basic_entity']);exit;*/

  //field_attach_form('questionnaire', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
    );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('questionnaire_edit_delete'),
    '#weight' => 200,
    );

  return $form;
}
/**
 * Validation handler for questionnaire_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function questionnaire_add_form_validate($form, &$form_state) {

  field_attach_form_validate('questionnaire', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits questionnaire_add_form information
 */
function questionnaire_add_form_submit($form, &$form_state) {
    //dsm($form_state);
    $matches = array();

    foreach($form_state['values'] as $key => $value) {
      if(strpos($key,'_field')) {
        $matches[$key] = array();
        $br = explode("_",$key);
        //$fv = array_shift(array_values($br));
        $fv = reset($br);
        if($form_state['values'][$key]) {
          $matches[$key]= array($form_state['values'][$key], $form_state['values']['textfield_'. $fv]);
        }
      }
    }
  //dsm($matches);               $form_state['input']['yes_reference']
    $data=serialize($matches);


    $op = explode("|",$form_state['input']['yes_reference']);
    $yes = (end($op));
    $ops = explode("|",$form_state['input']['no_reference']);
    $no = (end($ops));
  /*print_r($yes);
  print_r($no);exit;*/
  $entity = $form_state['values']['basic_entity'];
  $entity->title = $form_state['values']['title'];
  $entity->text = $form_state['values']['body'];
  $entity->choice = !($form_state['values']['questionnaire_type']) ? 1 : 0;
  $entity->criteria = $form_state['values']['questionnaire_type'] ? 1 : 0;
  $entity->criteria_data = $data;
  $entity->tokenized_criteria = $form_state['values']['tokenized_criteria'];
  $entity->yes_quid = empty($form_state['input']['yes_reference']) ? null : $yes;
  $entity->no_quid = empty($form_state['input']['no_reference']) ? null : $no;
  //field_attach_submit('questionnaire', $entity, $form, $form_state);
  $entity = questionnaire_save($entity);
  $form_state['redirect'] = 'questionnaire/' . $entity->qid;
}

/**
 * We save the entity by calling the controller.
 */
function questionnaire_save(&$entity) {
  return entity_get_controller('questionnaire')->save($entity);
}

/**
 *
 */
function questionnaire_edit_delete($form, &$form_state) {
  //dsm($form_state);
  $entity = $form_state['values']['basic_entity'];
  entity_delete($entity,'questionnaire');
  drupal_set_message(t('The entity %title (ID %id) has been deleted',
    array('%title' => $entity->title, '%id' => $entity->qid))
  );
  $form_state['redirect'] = '';
}
/**
 *
 */
function questionnaire_view($entity, $view_mode = 'question') {
    //dsm($entity);
    //return 'heelo';
 // dsm($view_mode);
 /*return "hello";*/

 $entity_type = 'questionnaire';
 $entity_id = entity_id($entity_type, $entity);

  //
  // Remove previously built content, if exists
  //
 $entity->content = array();

 $entity->title = filter_xss($entity->title);

  //
  // Build the fields content
  //
 field_attach_prepare_view($entity_type, array($entity_id => $entity), $view_mode);
 entity_prepare_view($entity_type, array($entity_id => $entity));

 $entity->content += field_attach_view($entity_type, $entity, $view_mode);
 return theme('questionnaire', array('entity' => $entity));

}

/*function guided_assistance_entity_view($entity, $entity_type= 'questionnaire', $view_mode='question') {
  field_attach_prepare_view($entity_type= 'questionnaire', $entity, $view_mode='question');
  entity_prepare_view(questionnaire, $entity);
  return $entity;
}*/
/**
 *
 */
function questionnaire_title($entity) {
  return t('Questionnaire: @title', array('@title' => $entity->title));
}

function questionnaire_edit_form($form, &$form_state, $questionnaire) {
// dsm ($questionnaire);
  return drupal_get_form('questionnaire_add_form', $questionnaire);
}

function ajax_append(&$form, &$form_state) {
//  dsm($form_state['complete form']['checkbox_field'][0]['#value']);  $form[#build_id]
//dsm ($form_state);dsm($form);
  global $mapping;
  $form['tokenized_criteria']['#value'] = null;
  if($form_state['input']['radio_field'] == 1) {
    if(!variable_get('key_2',0)) {

     $a = $mapping['radios'];
     $form['tokenized_criteria']['#value'] .= $a. ' ';
     variable_set('key_2', 1);
        //return $form['tokenized_criteria'];
   }
 }
 if($form_state['input']['checkbox_field'] == 1) {
  if(!variable_get('key_0',0)) {

   $a = $mapping['checkboxes'];
   $form['tokenized_criteria']['#value'] .= $a.' ';
   variable_set('key_0', 1);
        //return $form['tokenized_criteria'];
 }
}
//dsm(arg(1));
if($form_state['input']['selectlist_field'] == 1)  {
  if(!variable_get('key_1',0)) {
   //$form['tokenized_criteria']['#value'] .=  'Select-list  ';
   $a = $mapping['select'];
   $form['tokenized_criteria']['#value'] .= $a.' ';
   variable_set('key_1', 1);
        //return $form['tokenized_criteria'];
 }
}
return $form['tokenized_criteria'];
}

function guided_assistance_form($form, &$form_state, $question) {
 print_r("in template function");
//dsm($question); >yes_quid
 $form_build = unserialize($question->criteria_data);
 //dsm($form_build);
 foreach ($form_build as $key => $value) {

  $options = ($form_build[$key]['1']);
  $list = explode("\n", $options);

  $list = array_map('trim', $list);

  $list = array_filter($list, 'strlen');
  $new_list = array();
  foreach ($list as $index => $value) {
    $new = explode('|',$value);
    $new_key = reset($new);
    $new_value = end($new);
    $new_list[$new_key] = $new_value;
  }


  $fb = explode("_",$key);
  $type =reset($fb);

  if($type == 'radio') {

    if($form_build[$key]['0']) {
      $form['radio_form'] = array(
        '#type' => 'radios',
        '#options' => $new_list,
        );
    }
  }

  elseif($type == 'checkbox') {
    if($form_build[$key]['0']) {
     $form['checkbox_form'] = array(
       '#type' => 'checkboxes',
       '#options' => $new_list,
       );
   }
 }
 elseif($type == 'selectlist') {
  if($form_build[$key]['0']) {
    $form['selectlist_form'] = array(
      '#type' => 'select',
      '#options' => $new_list,
      );
  }

}

}
$form['yes_questionnaire'] = array(
    '#type' => 'value',
    '#value' => $question->yes_quid,
  );
$form['no_questionnaire'] = array(
    '#type' => 'value',
    '#value' => $question->no_quid,
  );
$form['submit'] = array(
  '#type' => 'submit',
  '#value' => t('Submit'),
  '#weight' => 100,
  );

return $form;
}
/*['complete form']['radio_form']['#value']
['complete form']['checkbox_form']['d']['#title']

['complete form']['radio_form']['a']['#title']*/
//['values']['radio_form']  ['complete form']['radio_form']['#type']
//type = $form_state['complete form'][$key]['#type']
function guided_assistance_form_submit($form, &$form_state) {
  //dsm($form_state);
  $copy;
  $input_value = array();
  foreach ($form_state['values'] as $key => $value) {
    $t = $form_state['complete form'][$key]['#type'];
    if(strpos($key,'_form')) {

      $new_key = $form_state['values'][$key];
      //dsm($new_key);
      if(is_array($new_key)) {
        //dsm('is array yes in');
        foreach ($new_key as $k => $v) {
          if($v) {
            $input_value[$t]=  $form_state['complete form'][$key]['#options'][$v] ;
            //dsm($input_value);
          }
        }
      }
      else {
        //$input_value[] =  $form_state['complete form'][$key]['#options'][$new_key];
        $input_value[$t] =  $form_state['complete form'][$key]['#options'][$new_key];
      }
    }
  }
   //dsm($input_value);if(is_array($input_value[$key]))
  $qid = arg(1);
  $result = db_select('questionnaire', 'q')
  ->fields('q',array('tokenized_criteria'))
  ->condition('qid', $qid,'=')
  ->execute()
  ->fetchAssoc();
  //dsm($result);
  global $mapping;
    $copy=$result['tokenized_criteria'];

    foreach ($mapping as $key => $value) {

      $copy = str_replace($value, $input_value[$key], $copy);
   }
   //dsm($copy);
    $op = eval("if($copy) return 1; else return 0;");
   //$op = eval("$copy?1:0;");
   //dsm($op);
   if($op) {
    dsm('yes');
    $yes = $form_state['values']['yes_questionnaire'];
    drupal_goto($base_url.'questionnaire/'.$yes);
   }
   else {
    dsm('no');
    $no = $form_state['values']['no_questionnaire'];
    drupal_goto($base_url.'questionnaire/'.$no);
   }

   }
